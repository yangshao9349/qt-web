﻿#include "mainwindow.h"

#include <QApplication>
#include <QWebEngineView>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QJsonDocument>
#include <QJsonObject>
#include <QDebug>
#include <QWebEngineSettings>
int main(int argc, char *argv[])
{





      QApplication a(argc, argv);

      QNetworkAccessManager manager;
         QUrl url("http://127.0.0.1:8080/test"); // 替换为你的URL
         QNetworkRequest request(url);

         QNetworkReply *reply = manager.get(request);

         QObject::connect(reply, &QNetworkReply::finished, [&]() {
             if (reply->error()) {
                 qDebug()  << "Error: " << reply->errorString();
             } else {
                 // 处理返回的数据
                 QString response = reply->readAll();
                  qDebug() << "Response: " << response;
             }
             reply->deleteLater();
         });

   QMainWindow window;
   QWebEngineView *view = new QWebEngineView(&window);
   QWebEngineSettings::globalSettings()->setAttribute(QWebEngineSettings::LocalStorageEnabled, true);



   view->setUrl(QUrl("http://localhost:8080/config"));
   window.setCentralWidget(view);
   window.resize(1024, 768);
   window.show();

       return a.exec();
}
